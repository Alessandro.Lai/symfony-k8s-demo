# Symfony + Kubernetes demo app

This is a Symfony 4 demo application used to demonstrate how to achieve [continuous deployment](https://en.wikipedia.org/wiki/Continuous_deployment) on Kubernetes. This demo is part of a talk presented at [SFDay 2019](https://2019.sfday.it/) and [Codemotion Milan 2019](https://events.codemotion.com/conferences/milan/2019).

## References
 * [Talk page on my personal site](https://alessandrolai.dev/talks/2019-sfday-symfony-loves-k8s/) (will update with video when available)
 * [Slides](https://alessandrolai.dev/slides/2019-10-symfony-loves-k8s-sfday/)
 * [Talk page on Joind.in](https://joind.in/talk/910a4) 
 * The structure of this repo and some Docker/GitLab CI tricks are taken from what was explained in [the previous talk](https://alessandrolai.dev/talks/2018-symfonyday-symfony-docker/).

## Contents
This is a brief explanation of the content of the folders of this repository:

 * `bin`, `config`, `public`, `src`, `var`, `vendor` are the classic folders of a Symfony 4 project
 * `charts` contains [Helm](https://helm.sh/) charts for this app 
 * `ci` contains some additional configuration for the [CI pipeline](https://gitlab.com/Alessandro.Lai/symfony-k8s-demo/pipelines); those files are included in `.gitlab-ci.yml`
 * `docker` contains the Dockerfile necessary to build all the Docker images used in this project
 * `k8s` contains a committed dump of the Helm charts; invoke `make helm-template` to regenerate them
 * `makefiles` contains configurations for the `make` command in this project
 * the Docker Compose configuration is splitted: the `docker-compose.override.yml.dist` is a boilerplate for a custom, uncommitted `docker-compose.override.yml` file, and `docker-compose.override.gitlab.yml` is instead used only in the CI builds
