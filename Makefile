start:
	running=$$(docker-compose ps php | grep -c "Up"); \
	if [ "$$running" -eq 0 ]; then \
		docker-compose up -d nginx; \
	fi;

shell: start
	@docker-compose exec php zsh

include makefiles/helm.mk
include makefiles/symfony.mk

bold := "\\033[1m"
normal := "\\033[0m"
boldunderline := "\\033[1m\\033[4m"

.SILENT:
